//
//  GoalType.swift
//  goal-post
//
//  Created by Sunimal Herath on 14/12/18.
//  Copyright © 2018 Sunimal Herath. All rights reserved.
//

import Foundation

enum GoalType: String{
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
