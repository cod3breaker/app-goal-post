//
//  UIViewControllerExt.swift
//  goal-post
//
//  Created by Sunimal Herath on 16/12/18.
//  Copyright © 2018 Sunimal Herath. All rights reserved.
//

import UIKit

extension UIViewController{
    func presentDetails(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    func dissmissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: true, completion: nil)
    }
}
